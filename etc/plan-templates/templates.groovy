plan(key: 'MAIN', name: 'Main') {
    project(key: 'HUMANDURATION', name: 'Human Duration library')

    repository(name: 'Human Duration library')

    trigger(type: 'polling', strategy: 'periodically', frequency: '600') {
        repository(name: 'Human Duration library')
    }
    trigger(type: 'cron', description: 'Run once a month to prevent deletion',
            cronExpression: '0 0 0 1 * ?')

    notification(type: 'Failed Jobs and First Successful', recipient: 'committers')

    // Maven release version variables
    variable(key: "maven.release.version", value: '')
    variable(key: "maven.next.version", value: '')

    branchMonitoring() {
        createBranch(matchingPattern: '.*')
        inactiveBranchCleanup(periodInDays: '30')
        deletedBranchCleanup(periodInDays: '7')
    }

    stage(name: 'Test Stage') {
        job(key: 'TEST', name: 'Build and test') {
            task(type: 'checkout', description: 'Checkout Default Repository')
            task(type: 'maven3', goal: 'clean install', mavenExecutable: 'Maven 3.3', buildJdk: 'JDK 1.8',
                    hasTests: 'true', testDirectory: '**/target/surefire-reports/*.xml, **/target/**/surefire-reports/*.xml')
        }
    }

    maven_release_stages_simple(manual: 'true', binaryModule: '{humanduration,humanduration-spring-boot-starter}',
            repositoryName: 'Human Duration library', versionVariable: 'maven.release.version',
            nextVersionVariable: 'maven.next.version', releaseExtraArguments: '', releaseExtraProfiles: '',
            environmentVariables: '', package: 'jar', mavenVersion: 'Maven 3.3', jdkVersion: 'JDK 1.8',
            tagPrefix: 'humanduration-', dryRun: 'false')
}
