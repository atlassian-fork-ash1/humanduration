package com.atlassian.humanduration.spring;

import com.atlassian.humanduration.HumanDuration;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.factory.config.CustomEditorConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.beans.PropertyEditorSupport;
import java.time.Duration;

@Configuration
public class HumanDurationAutoConfiguration {

    @Bean
    public HumanDurationToDurationConverter humanDurationAutoconverter() {
        return new HumanDurationToDurationConverter();
    }

    @Bean
    public static CustomEditorConfigurer customEditorConfigurer() {
        CustomEditorConfigurer configurer = new CustomEditorConfigurer();
        PropertyEditorRegistrar propertyEditorRegistrar = registry ->
                registry.registerCustomEditor(
                        Duration.class,
                        new PropertyEditorSupport() {
                            @Override
                            public void setAsText(String text) throws IllegalArgumentException {
                                setValue(HumanDuration.stringToDuration(text));
                            }
                        });
        configurer.setPropertyEditorRegistrars(new PropertyEditorRegistrar[]{propertyEditorRegistrar});
        return configurer;
    }
}
