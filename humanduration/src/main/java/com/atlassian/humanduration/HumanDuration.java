package com.atlassian.humanduration;

import java.time.Duration;
import java.time.temporal.TemporalUnit;
import java.util.regex.Pattern;

import static java.time.temporal.ChronoUnit.*;

public class HumanDuration {
    private static final Pattern ALPHANUMERIC_BOUNDARY_PATTERN = Pattern.compile("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
    private static final Pattern WHITESPACES_PATTERN = Pattern.compile("\\s+");

    /**
     * Parses {@link Duration} from the human readable {@link String} representation.
     * <p>
     * Human readable duration is a sequence of pairs of positive numbers followed by their time units.
     * They <i>might be</i> delimited with whitespace symbols, but the delimiters are not required.
     * <p>
     * For example:
     * <ul>
     * <li>{@code 1 hour 2 minutes 3 seconds} is parsed to {@link Duration} of 3723 seconds
     * <li>{@code 3h2m1s} is parsed to {@link Duration} of 10921 seconds
     * </ul>
     * <p>
     * Note that the durations of a day, week, month and year are estimated
     * and don't track daylight saving time or leap seconds.
     * <p>
     * Following time units and their textual representations are supported:
     * <pre>
     *  Time Unit        Textual representation                       Notes
     *  ---------        ----------------------                       -----
     *  nanosecond       ns ; nanos ; nanosecond ; nanoseconds
     *  microsecond      micros ; microsecond ; microseconds
     *  millisecond      ms ; millis ; millisecond ; milliseconds
     *  second           s ; second ; seconds
     *  minute           m ; minute ; minutes
     *  hour             h ; hour ; hours
     *  day              d ; day ; days                               Duration of a day is exactly 24 hours
     *  week             w ; week ; weeks                             Duration of a week is exactly 7 days
     * </pre>
     *
     * @throws MalformedHumanDurationException if the string is not a valid representation of a human readable duration.
     */
    public static Duration stringToDuration(String input) throws MalformedHumanDurationException {
        if (input == null || input.isEmpty()) {
            throw new MalformedHumanDurationException("input is empty");
        }
        try {
            String compactLowerCase = WHITESPACES_PATTERN.matcher(input).replaceAll("").toLowerCase();
            String[] parts = ALPHANUMERIC_BOUNDARY_PATTERN.split(compactLowerCase);

            if (parts.length == 1) {
                if (Integer.valueOf(parts[0]) == 0) {
                    return Duration.ZERO;
                }
                throw new MalformedHumanDurationException("non-zero value should have a time unit");
            }

            // there should be a time unit for each number in the sequence
            if (parts.length % 2 == 0) {
                Duration result = Duration.ZERO;
                for (int i = 0; i < parts.length; i += 2) {
                    Duration partDuration = parseUnitDuration(parts[i + 1]).multipliedBy(Long.parseLong(parts[i]));
                    result = result.plus(partDuration);
                }
                return result;
            }

            throw new MalformedHumanDurationException();
        } catch (NumberFormatException e) {
            throw new MalformedHumanDurationException("invalid number format", e);
        }
    }

    private static Duration parseUnitDuration(String input) {
        return parseUnit(input).getDuration();
    }

    private static TemporalUnit parseUnit(String input) {
        switch (input) {
            case "ns":
            case "nanos":
            case "nanosecond":
            case "nanoseconds":
                return NANOS;
            case "micros":
            case "microsecond":
            case "microseconds":
                return MICROS;
            case "ms":
            case "millis":
            case "millisecond":
            case "milliseconds":
                return MILLIS;
            case "s":
            case "second":
            case "seconds":
                return SECONDS;
            case "m":
            case "minute":
            case "minutes":
                return MINUTES;
            case "h":
            case "hour":
            case "hours":
                return HOURS;
            case "d":
            case "day":
            case "days":
                return DAYS;
            case "w":
            case "week":
            case "weeks":
                return WEEKS;
            default:
                throw new MalformedHumanDurationException(String.format("unknown unit of time '%s'", input));
        }
    }
}
