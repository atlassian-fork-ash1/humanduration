package com.atlassian.humanduration;

public class MalformedHumanDurationException extends RuntimeException {
    private static final String MESSAGE_PATTERN = "Malformed duration in the input%s. " +
            "Expected a sequence of pairs of positive numbers followed by their time units.";

    public MalformedHumanDurationException() {
        super(messageWithDescription(null));
    }

    public MalformedHumanDurationException(String description) {
        super(messageWithDescription(description));
    }

    public MalformedHumanDurationException(String description, Throwable cause) {
        super(messageWithDescription(description), cause);
    }

    private static String messageWithDescription(String maybeDescription) {
        return String.format(MESSAGE_PATTERN, maybeDescription == null ? "" : ": " + maybeDescription);
    }
}
